case class ChineseChar(char: String,
                       strokes: Int,
                       decompType: String,
                       comp1: String,
                       comp1strokes: Int,
                       comp2: String,
                       comp2strokes: Int,
                       radical: String) {

  def hasComp1 = comp1 != "*" && comp1 != "?" && comp1 != ""
  def hasComp2 = comp2 != "*" && comp2 != "?" && comp2 != ""
}
