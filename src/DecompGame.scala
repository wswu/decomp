import java.awt._
import java.awt.event.{ActionEvent, ActionListener, MouseAdapter, MouseEvent}
import java.io.IOException
import javax.swing._

import scala.util.Random

class DecompGame extends JFrame {

  val wordDict = new CedictReader().read(getClass.getClassLoader.getResourceAsStream("cedict.txt"))
  val charDict = new DecompReader().read(getClass.getClassLoader.getResourceAsStream("decomp.html"))

  var currentChar = ""
  val leftRadicals = new JPanel(new GridLayout(2, 2))
  val rightRadicals = new JPanel(new GridLayout(2, 2))
  val leftRadical = new JLabel("", SwingConstants.RIGHT)
  val rightRadical = new JLabel("")
  var chineseFont: Font = null

  val targetLabel = new JLabel("", SwingConstants.CENTER)

  val infoLabel = new JTextArea("") {
    setEditable(false)
    setBackground(leftRadical.getBackground)
    setForeground(leftRadical.getForeground)
    setWrapStyleWord(true)
  }

  def setupFont(): Unit = {
    try {
      chineseFont = Font.createFont(Font.TRUETYPE_FONT, getClass.getClassLoader.getResourceAsStream("SourceHanSans-Regular.ttc"))
      val ge = GraphicsEnvironment.getLocalGraphicsEnvironment
      ge.registerFont(chineseFont)
    } catch {
      case e: IOException =>
      case e: FontFormatException =>
        println("could not load font")
        chineseFont = new Font(Font.SANS_SERIF, Font.PLAIN, 24)
    }
  }

  def initGUI() {
    setTitle("Radical Game")
    setSize(480, 480)
    setLayout(new GridBagLayout())
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)

    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName)

    targetLabel.setFont(chineseFont.deriveFont(28f))

    infoLabel.setFont(chineseFont.deriveFont(20f))

    val radicalPanel = new JPanel(new FlowLayout())
    radicalPanel.add(leftRadicals)
    radicalPanel.add(rightRadicals)

    val charPanel = new JPanel(new GridLayout(1, 2))
    leftRadical.setFont(chineseFont.deriveFont(72f))
    rightRadical.setFont(chineseFont.deriveFont(72f))
    charPanel.add(leftRadical)
    charPanel.add(new JLabel("+", SwingConstants.CENTER) {
      setFont(getFont.deriveFont(24f))
    })
    charPanel.add(rightRadical)

    val middlePanel = new JPanel(new BorderLayout())
    middlePanel.add(charPanel, BorderLayout.CENTER)
    middlePanel.add(radicalPanel, BorderLayout.SOUTH)


    val bottomPanel = new JPanel()
    val newGameBtn = new JButton("New Game")
    newGameBtn.addActionListener(new ActionListener() {
      override def actionPerformed(e: ActionEvent) {
        newGame()
      }
    })
    bottomPanel.add(newGameBtn)

    val checkBtn = new JButton("Check")
    checkBtn.addActionListener(new ActionListener() {
      override def actionPerformed(e: ActionEvent) {
        check()
      }
    })
    bottomPanel.add(checkBtn)

    add(targetLabel, new GridBagConstraints() {
      gridx = 0
      gridy = 0
      fill = GridBagConstraints.HORIZONTAL
      gridwidth = 2
    })

    add(charPanel, new GridBagConstraints() {
      gridx = 0
      gridy = 1
      fill = GridBagConstraints.VERTICAL
      gridwidth = 2
      weightx = 0.5
      weighty = 0.5
    })

    add(radicalPanel, new GridBagConstraints() {
      gridx = 0
      gridy = 2
      gridwidth = 2
      weightx = 0.5
      weighty = 0.5
    })

    add(infoLabel, new GridBagConstraints() {
      gridx = 0
      gridy = 3
      gridwidth = 2
    })

    add(leftRadicals, new GridBagConstraints() {
      gridx = 0
      gridy = 4
      fill = GridBagConstraints.HORIZONTAL
      weightx = 0.5
      gridwidth = 1
    })

    add(rightRadicals, new GridBagConstraints() {
      gridx = 1
      gridy = 4
      gridwidth = 1
      weightx = 0.5
      fill = GridBagConstraints.HORIZONTAL
    })

    add(bottomPanel, new GridBagConstraints() {
      gridx = 0
      gridy = 5
      gridwidth = 2
    })
  }

  def newGame() {
    leftRadical.setText("")
    rightRadical.setText("")
    infoLabel.setText("")

    do {
      currentChar = charDict.keys.toList(Random.nextInt(charDict.size))
    } while (!wordDict.contains(currentChar))

    val defin = wordDict(currentChar).definitions.head
    targetLabel.setText(defin)

    val chinese = charDict(currentChar)
    val possRight = charDict.values.filter(x => x.comp1 == chinese.comp1 && x.hasComp2 && x.comp2.length == 1 && x.char != currentChar).map(_.comp2).toSeq
    val possLeft = charDict.values.filter(x => x.comp2 == chinese.comp2 && x.hasComp1 && x.comp1.length == 1 && x.char != currentChar).map(_.comp1).toSeq

    val rights = Random.shuffle(Random.shuffle(possRight).take(3) :+ chinese.comp2)
    val lefts = Random.shuffle(Random.shuffle(possLeft).take(3) :+ chinese.comp1)

    leftRadicals.removeAll()
    for (rad <- lefts) {
      val label = new JLabel(rad, SwingConstants.CENTER)
      label.setFont(chineseFont.deriveFont(48f))
      label.addMouseListener(new MouseAdapter() {
        override def mousePressed(e: MouseEvent) {
          leftRadical.setText(label.getText)
          infoLabel.setText("")
        }
      })
      leftRadicals.add(label)
    }

    rightRadicals.removeAll()
    for (rad <- rights) {
      val label = new JLabel(rad, SwingConstants.CENTER)
      label.setFont(chineseFont.deriveFont(48f))
      label.addMouseListener(new MouseAdapter() {
        override def mousePressed(e: MouseEvent) {
          rightRadical.setText(label.getText)
          infoLabel.setText("")
        }
      })
      rightRadicals.add(label)
    }

    repaint()
    revalidate()
  }

  def check() {
    val comp1 = leftRadical.getText
    val comp2 = rightRadical.getText
    if (comp1.length == 0 || comp2.length == 0)
      return

    val char = charDict.values.find(ce => ce.comp1 == comp1 && ce.comp2 == comp2)
    char match {
      case Some(c) =>
        if (c.char == currentChar) {
          infoLabel.setText("Correct! The character is " + c.char)
        } else {
          val dictEntry = wordDict.getOrElse(c.char, null)
          if (dictEntry == null)
            infoLabel.setText("That's not a word!")
          else
            infoLabel.setText("You made another word!\n" + c.char + " means " + dictEntry.definitions.head)
        }

      case None =>
        infoLabel.setText("I don't think that is a word!")
    }
  }
}

object Game extends App {
  val g = new DecompGame()
  g.setupFont()
  g.initGUI()
  g.setVisible(true)
}
