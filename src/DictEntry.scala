case class DictEntry(simp: String,
                     trad: String,
                     pinyin: String,
                     definitions: Seq[String]) {
}
