import java.io.InputStream

import scala.io.Source

class DecompReader {

  def read(is: InputStream) = {
    Source.fromInputStream(is).getLines()
      .filter(x => x.startsWith("        ") && x.trim.length != 0)
      .map { line =>

      // columns are as follows:
      // char strokes decompType comp1(may be blank) comp1strokes verify1(?, *, or empty) comp2 comp2strokes verify2 kangjie radical

      val arr = line.trim().split("\\s+")
      val char = arr(0)
      val strokes = arr(1).toInt
      val decompType = arr(2)

      var i = 3

      val comp1 =
        if (arr(3).matches("\\d+")) {
          ""
        } else {
          i += 1
          arr(3)
        }


      val comp1strokes = arr(i).toInt
      i += 1

      if (arr(i) == "?" || (arr(i) == "*" && !arr(i + 1).matches("\\d+"))) {
        i += 1
      }

      val comp2 =
        if (arr(i).matches("\\d+")) {
          // empty comp2
          ""
        } else {
          i += 1
          arr(i - 1)
        }

      val comp2strokes = arr(i).toInt

      val radical = arr(arr.length - 1)

      char -> ChineseChar(char, strokes, decompType, comp1, comp1strokes, comp2, comp2strokes, radical)
    }.toMap
  }

}
