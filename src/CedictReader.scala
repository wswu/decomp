import java.io.InputStream

import scala.io.Source

class CedictReader {

  def read(is: InputStream): Map[String, DictEntry] = {
    val regex = """(\S+) (\S+) \[(.+)\] /(.+)/""".r

    Source.fromInputStream(is).getLines()
      .filterNot(_.startsWith("#"))
      .map {
      case regex(trad, simp, pinyin, defs) =>
        simp -> DictEntry(simp, trad, pinyin, defs.split("/"))
    }.toMap
  }

}